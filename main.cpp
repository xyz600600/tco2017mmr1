#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <tuple>
#include <array>
#include <cstdint>
#include <cassert>
#include <queue>
#include <algorithm>
#include <bitset>
#include <set>
#include <chrono>

typedef long long ll;

using namespace std;

#define TEST

// #define LOG

typedef unsigned long long int ulli;

unsigned long long int getCycle()
{
  unsigned int low, high;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return ((ulli)low) | ((ulli)high << 32);
}
const double cycle_per_sec = 2.5e9;

double getTime(ulli begin_cycle)
{
  return (double)(getCycle() - begin_cycle) / cycle_per_sec;
}

template<typename T>
class SegmentTree
{
private:
  const T NONE;
  const int32_t frontIndex;
  array<T, 50000> ary;

  int32_t minpow2(const int32_t n) const
  {
	int32_t ans = 1;
	while(ans < n)
	{
	  ans *= 2;
	}
	return ans;
  }
  
public:
  SegmentTree(const int32_t NE, const T _NONE)
	: NONE(_NONE)
	, frontIndex(minpow2(NE))
  {
	fill(ary.begin(), ary.begin() + (frontIndex + 1) * 2, NONE);
  }

  T &top()
  {
	return ary[1];
  }

  void update(const int32_t edgeId, const T val)
  {
	int32_t k = edgeId + frontIndex;
	ary[k] = val;
	while(k > 1)
	{
	  k = k / 2;
	  ary[k] = min(ary[k * 2], ary[k * 2 + 1]);
	}
  }

  void init()
  {
	fill(ary.begin(), ary.end(), NONE);
  }
};

struct Edge {
  double rate;
  int32_t id;

  Edge()
	: rate(0)
	, id(0){}
  
  Edge(double rate,
	   int16_t id)
	: rate(rate)
	, id(id){}
};

const Edge POS_INF(1e100, -1);
const Edge NEG_INF(-1e100, -1);

bool operator<(Edge e1, Edge e2)
{
  return e1.rate < e2.rate;
}

bool operator>(Edge e1, Edge e2)
{
  return e1.rate > e2.rate;
}

struct Point
{
  int32_t y;
  int32_t x;
  bool visited;

  Point(int16_t y, int16_t x)
	: y(y)
	, x(x)
	, visited(true){}

  Point()
	: y(0)
	, x(0)
	, visited(false){}
};

bool operator!=(const Point p1, const Point p2)
{
  return p1.y != p2.y || p1.x != p2.x;
}

double distanceOf(const Point p1, const Point p2)
{
  const double dx = p1.x - p2.x;
  const double dy = p1.y - p2.y;
  return sqrt(dx * dx + dy * dy);
}

double distanceOf(const double dy, const double dx)
{
  return sqrt(dy * dy + dx * dx);
}

struct State
{
  vector<Point> ps;
  double globalMaxRate;
  double globalMinRate;
  uint64_t hash;

  State(const int32_t NV)
  {
	ps.resize(NV);
	globalMinRate = 1e100;
	globalMaxRate = -1e100;
	hash = 0;
  }
  
  Point &operator[](int32_t idx) 
  {
	return ps[idx];
  }

  const Point &operator[](int32_t idx) const
  {
	return ps[idx];
  }

  size_t size() const
  {
	return ps.size();
  }

  void copyFrom(const State &src)
  {
	copy(src.ps.begin(), src.ps.end(), ps.begin());
	globalMaxRate = src.globalMaxRate;
	globalMinRate = src.globalMinRate;
	hash = src.hash;
  }  
};

struct BaseEdge
{
  const int32_t from;
  const int32_t to;
  const int32_t length;
  const int32_t id;

  BaseEdge(int32_t from, int32_t to, int32_t length, int32_t id)
	: from(from)
	, to(to)
	, length(length)
	, id(id){}
};

struct EdgeManager
{
  // (nodeId, edgeId)
  vector<vector<pair<int32_t, int32_t>>> graph;
  vector<BaseEdge> edges;
  SegmentTree<Edge> maxTree;
  SegmentTree<Edge> minTree;

  void init()
  {
	maxTree.init();
	minTree.init();
  }
  
  EdgeManager(const vector<int> &vs, const int NV)
	: maxTree(vs.size() / 3u, POS_INF)
	, minTree(vs.size() / 3u, POS_INF)
  {
	graph.resize(NV);
	int32_t id = 0;
	assert(vs.size() % 3 == 0);
	for(size_t i = 0; i < vs.size() / 3u; i++)
	{
	  int32_t from = vs[3 * i];
	  int32_t to   = vs[3 * i + 1];
	  int32_t length = vs[3 * i + 2];
	  BaseEdge e(from, to, length, id);
	  edges.push_back(e);
	  graph[from].push_back(make_pair(to, id));
	  graph[to].push_back(make_pair(from, id));
	  id++;
	}
  }
  
  const Edge getMaxEdge(int32_t pos)
  {
  	if(pos == 0) {
	  return maxTree.top();
	} else {
	  auto e = maxTree.top();
	  maxTree.update(e.id, POS_INF);
	  auto ret = maxTree.top();
	  maxTree.update(e.id, e);
	  return ret;
	}
  }

  const Edge getMinEdge(int32_t pos)
  {
  	if(pos == 0) {
	  return minTree.top();
	} else {
	  auto e = minTree.top();
	  minTree.update(e.id, POS_INF);
	  auto ret = minTree.top();
	  minTree.update(e.id, e);
	  return ret;
	}
  }

  void registerEdge(const int32_t edgeId, const double distance)
  {
	const double rate = distance / edges[edgeId].length;

	minTree.update(edgeId, Edge(rate, edgeId));
	maxTree.update(edgeId, Edge(-rate, edgeId));
  }

  void setMinMaxRate(const int32_t nodeId, double &minRate, double &maxRate)
  {
	maxRate = -maxTree.top().rate;
	minRate = minTree.top().rate;
  }
};

// --------------------
// viewer用のlog出力関数

#ifndef LOG

void SET_GRAPH(const EdgeManager &em){}
void SET_COORD(const int id, const double y, const double x){}
void PLOT_CONTOUR(const int id){}
void PLOT_POINT_AROUND(const int id){}
void SAVE_SNAPSHOT(string s){}
void PLOT_GRAPH(){}

#else

// 最初にグラフのエッジとノードの関係を定義
void SET_GRAPH(const EdgeManager &em)
{
  cerr << "graph" << endl;
  cerr << em.graph.size() << endl;
  for(auto vs : em.graph)
  {
	for(auto v : vs)
	{
	  cerr << v.first << " " << em.edges[v.second].length << " ";
	}
	cerr << endl;
  }
}

// idの座標を(y, x)に設定
void SET_COORD(const int id, const double y, const double x)
{
  cerr << "set" << endl;
  cerr << id << " " << y << " " << x << endl;
}

// 既に座標が確定しているグラフをplot
void PLOT_GRAPH()
{
  cerr << "all" << endl;
}

// idのrateに関する目的関数をplot
void PLOT_CONTOUR(const int id)
{
  cerr << "contour" << endl;
  cerr << id << endl;
}

// idの点とその隣接する点も表示
void PLOT_POINT_AROUND(const int id)
{
  cerr << "pointaround" << endl;
  cerr << id << endl;
}

// PLOT関数が呼ばれた状態でsavefig
void SAVE_SNAPSHOT(string title)
{
  cerr << "snapshot" << endl;
  cerr << title << endl;
}

#endif

// --------------------

typedef bitset<1048576> BitMap;

// 点を置いた場所を覚えておく
array<array<bool, 2101>, 2101> usedTable;

// optimizePositionを評価する時，既に探索済の点を再評価しなくて済むように
array<array<uint32_t, 2101>, 2101> visitedTable;
array<array<double, 2101>, 2101> evalTable;
uint32_t visitedId;

class GraphDrawing
{
public:
  vector<int> plot(const int NV, const vector<int> &edges);
private:
  void initialize(const int32_t NV);
  void forceDesiablePosition(State &state, int32_t fixedId,
							 int32_t moveId, int32_t length,
							 double desiableRate);
  void initializePosition(EdgeManager &em, State &state);
  void initializePositionRandomly(EdgeManager &em, State &state);
  pair<int32_t, int32_t> selectForceChangeId(EdgeManager &em, State &state);
  void optimize2(EdgeManager &em, State &state);
  void setAnswer(const State &s, vector<int> &ret);
  double clip(double lower, double val, double upper);
  void decideVisitOrder(vector<int32_t> &vs, const vector<vector<pair<int32_t, int32_t>>> &graph,
						const int32_t initId);
  void selectUnvisitedPoint(int32_t &y, int32_t &x, int32_t s, int32_t e);
  void shrink(State &s);
  double evalNorm2(const State &s, const EdgeManager &em, int32_t id);
  pair<Point, Point> getRange(const State &state);
  double getRejectProb(const double scoreDiff, const double threashold1, const double threashold2);
  double getInitialT(const State &s, const EdgeManager &em);
  
  mt19937 mt;
  ulli start;
  double idealRate;
  double T;
};

void GraphDrawing::shrink(State &s)
{
  for(auto &v : usedTable)
  {
	fill(v.begin(), v.end(), false);
  }

  int32_t minY = 10000;
  int32_t minX = 10000;
  int32_t maxY = 0;
  int32_t maxX = 0;

  for(const auto &p : s.ps)
  {
	minY = min(minY, p.y);
	minX = min(minX, p.x);
	maxY = max(maxY, p.y);
	maxX = max(maxX, p.x);
  }

  cerr << "[" << minX << ", " << maxX <<"] x [" << minY << ", " << maxY << "]" << endl;

  double rate = 700.0 / max(maxY - minY, maxX - minX);
  for(auto &p : s.ps)
  {
	p.y = clip(0, (p.y - minY) * rate, 700);
	p.x = clip(0, (p.x - minX) * rate, 700);
	
	if(usedTable[p.y][p.x])
	{
	  selectUnvisitedPoint(p.y, p.x, 0, 700);
	}
	usedTable[p.y][p.x] = true;
  }
}

pair<Point, Point> GraphDrawing::getRange(const State &state)
{
  Point minP(10000, 10000), maxP(-10000, -10000);

  for(auto &v : state.ps)
  {
	minP.y = min(v.y, minP.y);
	minP.x = min(v.x, minP.x);
	maxP.y = max(v.y, maxP.y);
	maxP.x = max(v.x, maxP.x);
  }
  return make_pair(minP, maxP);  
}

void GraphDrawing::initializePositionRandomly(EdgeManager &em, State &state)
{
  uniform_int_distribution<int32_t> dist(0, 2100);
  
  const int32_t NV = em.graph.size();
  for(size_t i = 0; i < NV; i++)
  {
	state[i].y = dist(mt);
	state[i].x = dist(mt);
	
	while(usedTable[state[i].y][state[i].x])
	{
	  selectUnvisitedPoint(state[i].y, state[i].x, 0, 2100);
	}
	usedTable[state[i].y][state[i].x] = true;
  }
}

// 単純化のため，higher is betterに変更してる.
double GraphDrawing::evalNorm2(const State &s, const EdgeManager &em, int32_t id)
{
  double ans = 0.0;
  for(auto &v : em.graph[id])
  {
	const double len = em.edges[v.second].length * idealRate;
	double val = (distanceOf(s[id], s[v.first]) - len) / len;
	ans += val * val;
  }
  return - ans / em.graph[id].size();
}

double GraphDrawing::getRejectProb(const double scoreDiff, const double threashold1, const double threashold2)
{
  const double remain = 1.0 - (getTime(start) - threashold2) / (threashold1 - threashold2) + 1e-6;
  return min(1.0, exp(scoreDiff / (T * remain)));
}

double GraphDrawing::getInitialT(const State &s, const EdgeManager &em)
{
  double ans = 0.0;
  for(int id = 0; id < em.graph.size(); id++)
  {
	double local_ans = 0.0;
	for(auto &v : em.graph[id])
	{
	  const double len = em.edges[v.second].length * idealRate;
	  const double diff = (distanceOf(s[id], s[v.first]) - len) / len;
	  local_ans += diff * diff;
	}
	local_ans /= em.graph[id].size();
	ans = max(ans, local_ans);
  }
  return - ans;
}

void GraphDrawing::optimize2(EdgeManager &em, State &state)
{
  const int32_t NV = em.graph.size();

  fill(state.ps.begin(), state.ps.end(), Point());
  initializePositionRandomly(em, state);

  State bestState(NV);
  double bestEval = 0.0;
  
  bestState.copyFrom(state);
  T = getInitialT(state, em);

  double elapsedTime = getTime(start);

  int time = 0;
  int time2 = 0;
  
  uniform_int_distribution<int32_t> randCoord(-128, 128);
  uniform_int_distribution<int32_t> randint(0, NV - 1);
  uniform_real_distribution<double> prob;

  const int iter = 1;
  
  for(int i = 1; i <= iter; i++)
  {
	const double threashold = 7.0 * i / iter;
	const double threashold2 = 9.5 * i / iter;
  
	while(elapsedTime < threashold)
	{
	  int32_t id = randint(mt);
	
	  const double rate = (threashold - getTime(start)) / threashold;
	  const int32_t ny = clip(0, state[id].y + randCoord(mt) * rate, 2100);
	  const int32_t nx = clip(0, state[id].x + randCoord(mt) * rate, 2100);
	  const int32_t by = state[id].y;
	  const int32_t bx = state[id].x;

	  const double evalBefore = evalNorm2(state, em, id);
	
	  state[id].y = ny;
	  state[id].x = nx;
	  const double evalAfter = evalNorm2(state, em, id);
	
	  if(evalAfter < evalBefore && getRejectProb(evalAfter - evalBefore, 0, threashold) > prob(mt))
	  {
		state[id].y = by;
		state[id].x = bx;
	  }

	  if(time % 100000 == 0) {
		double ans = 0;
		for(int32_t id = 0; id < NV; id++) {
		  ans += evalNorm2(state, em, id);
		}
	  }
	
	  time++;
	  if(time & 1023 == 1023)
	  {
		elapsedTime = getTime(start);
	  }
	}

	// register edgeCost
	for(auto &e : em.edges)
	{
	  const double dist = distanceOf(state[e.from], state[e.to]);
	  em.registerEdge(e.id, dist);
	}

	T = 10.0;
	int continue_fail = 0;
  
	while(elapsedTime < threashold2)
	{
	  time2++;
	  const int32_t pos = randCoord(mt) < 0 ? 0 : 1;
	  const auto e = randCoord(mt) < 0 ? em.getMinEdge(pos) : em.getMaxEdge(pos);
	  const int32_t id = randCoord(mt) < 0 ? em.edges[e.id].from : em.edges[e.id].to;

	  const int32_t by = state[id].y;
	  const int32_t bx = state[id].x;

	  const double my = ((double)state[em.edges[e.id].from].y + (double)state[em.edges[e.id].to].y) / 2;
	  const double mx = ((double)state[em.edges[e.id].from].x + (double)state[em.edges[e.id].to].x) / 2;

	  const double evalBefore = - em.getMinEdge(0).rate / em.getMaxEdge(0).rate;
	
	  const double rate = distanceOf(state[em.edges[e.id].from], state[em.edges[e.id].to]) / em.edges[e.id].length;
	  const double diffRate = 1.2 + (prob(mt) - 0.5) * 0.15;
	  const double localIdealRate = rate < idealRate ? max(idealRate, rate * diffRate) : min(idealRate, rate / diffRate);

	  const int32_t ny = clip(0, (int32_t)(my + (by - my) * localIdealRate / rate), 2100);
	  const int32_t nx = clip(0, (int32_t)(mx + (bx - mx) * localIdealRate / rate), 2100);

	  state[id].y = ny;
	  state[id].x = nx;

	  for(auto &v : em.graph[id])
	  {
		const double dist = distanceOf(state[id], state[v.first]);
		em.registerEdge(v.second, dist);
	  }
	  const double evalAfter = - em.getMinEdge(0).rate / em.getMaxEdge(0).rate;

	  // cerr << evalAfter << endl;
	  
	  if(evalAfter < evalBefore && getRejectProb(evalAfter - evalBefore, threashold, threashold2) > prob(mt))
	  {
		state[id].y = by;
		state[id].x = bx;

		for(auto &v : em.graph[id])
		{
		  const double dist = distanceOf(state[id], state[v.first]);
		  em.registerEdge(v.second, dist);
		}
		continue_fail++;
	  }
	  else if(bestEval < evalAfter)
	  {
		// cerr << evalAfter << endl;
		bestEval = evalAfter;
		bestState.copyFrom(state);
		continue_fail = 0;
	  }

	  if(continue_fail == 50000) 
	  {
	    state.copyFrom(bestState);
	    continue_fail = 0;
	  }
	
	  if(time & 63 == 63)
	  {
		elapsedTime = getTime(start);
	  } 
	}
	state.copyFrom(bestState);
  }
  cerr << "Time = " << time << endl;
  cerr << "Time2 = " << time2 << endl;

  shrink(state);
}

// 引き寄せる
void GraphDrawing::forceDesiablePosition(State &state, int32_t fixedId,
										 int32_t moveId, int32_t length,
										 double desiableRate)
{
  // cerr << "FORCE" << endl;
  const double distance = distanceOf(state[fixedId], state[moveId]);
  const double rate = distance / length;
  const double r = desiableRate / rate;

  usedTable[state[moveId].y][state[moveId].x] = false;
  
  state[moveId].y = clip(0, (1 - r) * state[fixedId].y + r * state[moveId].y, 2100);
  state[moveId].x = clip(0, (1 - r) * state[fixedId].x + r * state[moveId].x, 2100);

  SET_COORD(moveId, state[moveId].y, state[moveId].x);
  PLOT_GRAPH();
  PLOT_POINT_AROUND(moveId);
  SAVE_SNAPSHOT("FORCE: plot id: " + to_string(moveId) + ", moveId = " + to_string(state[moveId].y) + ", " + to_string(state[moveId].x));
  
  PLOT_POINT_AROUND(moveId);
  SAVE_SNAPSHOT("FORCE: plot id: " + to_string(moveId) + ", moveId = " + to_string(state[moveId].y) + ", " + to_string(state[moveId].x));	
  
  if(usedTable[state[moveId].y][state[moveId].x])
  {
	selectUnvisitedPoint(state[moveId].y, state[moveId].x, 0, 2100);
  }
  usedTable[state[moveId].y][state[moveId].x] = true;
}

int32_t pack(int32_t y, int32_t x)
{
  return (y << 10) + x;
}

void unpack(int32_t &y, int32_t &x, int32_t yx)
{
  y = (yx >> 10);
  x = yx & 0b1111111111;
}

void GraphDrawing::selectUnvisitedPoint(int32_t &y, int32_t &x, int32_t s, int32_t e)
{
  for(int32_t l = 1; l <= 50; l++)
  {
	for(int32_t df = -l; df <= l; df++)
	{
	  vector<int32_t> xs = {x + l, x - l, x + df, x + df};
	  vector<int32_t> ys = {y + df, y + df, y + l, y - l};
	  
	  for(int i = 0; i < 4; i++)
	  {
		if(s <= ys[i] && ys[i] <= e &&
		   s <= xs[i] && xs[i] <= e &&
		   !usedTable[ys[i]][xs[i]])
		{
		  y = ys[i];
		  x = xs[i];
		  return;
		}
	  }
	}
  }
}

double GraphDrawing::clip(double lower, double val, double upper)
{
  return max(lower, min(upper, val));
}

void GraphDrawing::setAnswer(const State &s, vector<int> &ret)
{
  ret.resize(2 * s.size());
  for(size_t i = 0; i < s.size(); i++)
  {
	int32_t y = clip(0, s[i].y, 700);
	int32_t x = clip(0, s[i].x, 700);
	
	ret[2 * i] = y;
	ret[2 * i + 1] = x;
  }
}

void GraphDrawing::initialize(const int32_t NV)
{
  start = getCycle();
  for(int i = 0; i <= 2100; i++)
  {
	fill(visitedTable[i].begin(), visitedTable[i].end(), 0);
	fill(usedTable[i].begin(), usedTable[i].end(), false);
  }
  visitedId = 0;
  idealRate = 0.9;
}

vector<int> GraphDrawing::plot(const int NV, const vector<int> &edges)
{
  initialize(NV);
  
  EdgeManager em(edges, NV);
  SET_GRAPH(em);
  
  State state(NV);
  optimize2(em, state);

  vector<int> ret;
  setAnswer(state, ret);

  return ret;
}

// -------8<------- end of solution submitted to the website -------8<-------
#ifdef TEST
template<class T> void getVector(vector<T>& v) {
  for (int i = 0; i < v.size(); ++i)
	cin >> v[i];
}

int main() {
  GraphDrawing gd;
  int N;
  cin >> N;
  // cerr << N << endl;
  int E;
  cin >> E;
  // cerr << E << endl;
  vector<int> edges(E);
  getVector(edges);
  // for(auto v : edges)
  // {
  // 	cerr << v << " ";
  // }
  // cerr << endl;
  // return 1;

  vector<int> ret = gd.plot(N, edges);
  cout << ret.size() << endl;
  for (int i = 0; i < (int)ret.size(); ++i)
	cout << ret[i] << endl;
  cout.flush();
}
#endif
