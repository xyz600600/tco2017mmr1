#!/bin/python

import sys
import re

def get_score(i):
    text = "".join(open("results/1/%d/stdout" % i).readlines())
    pattern = re.compile("Score = (0\.\d+)")
    result = re.search(pattern, text)
    return float(result.group(1))

size = int(sys.argv[1])
    
print "ave: " + str(sum(map(get_score, range(1, size + 1))) / size)
