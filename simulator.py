#!/bin/python

import sys

from pylab import *

class Edge:
    def __init__(self, to, length):
        self.to = to
        self.length = length

class Point:
    def __init__(self, y, x):
        self.y = y
        self.x = x
        self.exist = False

graph = []
coords = []

range_from = 0
range_to   = 2100

def getTarget(xs, ys, rs):

    def f(ix, iy):
        minTable = zeros(ix.shape) + 1
        maxTable = zeros(ix.shape) + 1

        for x, y, r in zip(xs, ys, rs):
            d = sqrt((ix - x) ** 2 + (iy - y) ** 2)
            rate = d / r
            
            minTable = minimum(minTable, rate)
            maxTable = maximum(maxTable, rate)
            
        return minTable / maxTable
    return f
    
def plot_contour(f):
    
    xs = arange(range_from, range_to, 2)
    ys = arange(range_from, range_to, 2)
    
    X, Y = meshgrid(xs, ys)
    Z = f(X, Y)

    minY, minX = np.unravel_index(argmax(Z), Z.shape)

    plot([xs[minX]], [ys[minY]], 'co')
                
    contour(X, Y, Z)
    colorbar()
    spring()    

def distanceOf(i, j):
    dy = (coords[i].y - coords[j].y)
    dx = (coords[i].x - coords[j].x)
    return sqrt(dy ** 2 + dx ** 2)
    
def simulate(filepath):
    data = map(lambda l: l.strip(), open(filepath).readlines())
    line = 0
    global_index = 0
    
    while line < len(data):
        if data[line] == "graph":
            line += 1
            NV = int(data[line])
            for i in range(1, NV + 1):
                edges = []
                inputs = map(int, data[line + i].strip().split())
                for v in zip(*[iter(inputs)]*2):
                    edges.append(Edge(*v))
                graph.append(edges)
                coords.append(Point(-1, -1))
            line += NV
            
        elif data[line] == "contour":
            line += 1
            index = int(data[line])
            xs = [coords[v.to].x for v in graph[index] if coords[v.to].exist]
            ys = [coords[v.to].y for v in graph[index] if coords[v.to].exist]
            rs = [v.length for v in graph[index] if coords[v.to].exist]
            if len(xs) >= 2:
                f = getTarget(xs, ys, rs)
                plot_contour(f)

        elif data[line] == "all":
            NV = len(graph)
            # edge
            for i, es in enumerate(graph):
                for e in es:
                    if coords[i].exist and coords[e.to].exist and i < e.to:
                        plot([coords[i].x, coords[e.to].x], [coords[i].y, coords[e.to].y], 'g-')
                        midX = (coords[i].x + coords[e.to].x) / 2
                        midY = (coords[i].y + coords[e.to].y) / 2
                        text(midX, midY + 10, "%d/%d\n(%.2f)" % (int(distanceOf(i, e.to)), e.length, (distanceOf(i, e.to) / e.length)), fontsize = 4)
                        
            # node
            xs = [coords[i].x for i in range(NV) if coords[i].exist]
            ys = [coords[i].y for i in range(NV) if coords[i].exist]
            plot(xs, ys, 'go')
            for i in [i for i in range(NV) if coords[i].exist]:
                text(coords[i].x, coords[i].y + 20, str(i), fontsize = 4, color = 'blue')
            
        elif data[line] == "set":
            line += 1
            id_y_x = data[line].strip().split()
            index = int(id_y_x[0])
            y = float(id_y_x[1])
            x = float(id_y_x[2])
            coords[index].y = y
            coords[index].x = x
            coords[index].exist = True
            
        elif data[line] == "pointaround":
            line += 1
            index = int(data[line])
            neighbors = [v for v in graph[index] if coords[v.to].exist]

            xs = [coords[v.to].x for v in neighbors]
            ys = [coords[v.to].y for v in neighbors]
            plot(xs, ys, 'bo')

            for e in graph[index]:
                if coords[index].exist and coords[e.to].exist:
                    plot([coords[index].x, coords[e.to].x], [coords[index].y, coords[e.to].y], 'g-')
                    midX = (coords[index].x + coords[e.to].x) / 2
                    midY = (coords[index].y + coords[e.to].y) / 2
                    text(midX, midY + 10, "%d/%d\n(%.2f)" % (int(distanceOf(index, e.to)), e.length, (distanceOf(index, e.to) / e.length)), fontsize = 4)

            plot([coords[index].x], [coords[index].y], 'ko')
            
        elif data[line] == "snapshot":
            line += 1
            title_lable = data[line]
            title(title_lable)
            xlim([range_from, range_to])
            ylim([range_from, range_to])
            savefig("fig/fig_%d.jpg" % global_index, dpi=200)
            print "fin %d th image" % global_index
            clf()
            global_index += 1
            
        else:
            print("Unknown Command: " + data[line])
            
        line += 1

if __name__ == "__main__":
    simulate(sys.argv[1])
