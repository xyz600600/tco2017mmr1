#!/bin/bash

g++ -std=c++11 main.cpp

size=100

parallel --progress --results results 'java -jar tester.jar -exec "./a.out" -seed {}' ::: `seq 1 $size`

python calc_score.py $size
